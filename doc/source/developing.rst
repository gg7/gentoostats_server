Development process
===================

Cloning the repository
----------------------

.. code:: bash

  # use this if you have a github account:
  git clone 'git@github.com:gg7/gentoostats_server.git'
  # or this if you don't:
  git clone 'https://github.com/gg7/gentoostats_server.git'

Bundled Portage?
----------------

I strongly recommend using the bundled Portage tree.

Warning: The repository contains a symlink (django_gentoostats/lib/portage)
which won't work under Windows.

.. code:: bash

  # I recommend using the bundled portage
  git submodule init && git submodule update

Environment variables
---------------------

.. code:: bash

  export DJANGO_SETTINGS_MODULE="gentoostats.settings.dev"
  export PYTHONPATH=".../gentoostats_server/django_gentoostats"

Management commands
-------------------

Note: You can also use django-admin.py instead of ./manage.py

Running the server: ::

  ./manage.py runserver_plus

Running the server with a profiler: ::

  ./manage.py runprofileserver

Database shell: ::

  ./manage.py dbshell

Testing code coverage: ::

  coverage run --source='.' ./manage.py runserver_plus
  coverage report

Virtualenv
----------

.. code:: bash

  pip freeze --local

Collecting static files
-----------------------

.. code:: bash

  ./manage.py collectstatic --noinput --ignore='.keep' -c
