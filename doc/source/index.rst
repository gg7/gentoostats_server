.. Gentoostats documentation master file, created by
   sphinx-quickstart on Tue Sep 10 23:25:06 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Gentoostats's documentation!
=======================================

Contents:

.. toctree::
   :maxdepth: 2

developing.rst

models.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

