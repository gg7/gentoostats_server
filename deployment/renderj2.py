#!/usr/bin/env python

import sys
import jinja2

vars = { "project_venv": "/srv/gentoostats/env"
       , "project_root": "/srv/gentoostats"
}

with open(sys.argv[1]) as f:
    template = jinja2.Template(f.read())
    print template.render(**vars)
