#!/bin/sh
set -e

# requires realpath

cd "$(dirname $(realpath --no-symlinks "$0"))"

if [[ -f 'gentoostats-env' ]]; then
    rm -r 'gentoostats-env'
fi

virtualenv --no-site-packages 'gentoostats-env'
cp 'requirements.txt' 'gentoostats-env/'
cd 'gentoostats-env'
source bin/activate
pip install -r 'requirements.txt'
