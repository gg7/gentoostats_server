About
=====

The Gentoostats project aims to collect and analyse various statistics about
Gentoo systems.

This repository is for the server of the project. You can find the client
[here](https://github.com/gg7/gentoostats/tree/gg7).

Installation
============

Gentoo systems
--------------

Required:
```text
root # emerge -av '=dev-lang/python-2.7' \
                  dev-python/virtualenv  \
                  dev-python/setuptools  \
                  dev-python/pip         \
                  dev-python/celery      \
                  dev-libs/geoip
```

Recommended database:
```text
root # emerge -av dev-db/postgresql-server
```

Make sure to create a suitable settings.py. You can use settings.py.example as
an example. Remember to modify the secret key, the database section, and the
installed apps section. If you **must** use MySQL (which I haven't tested), make
sure to use the InnoDB engine, as MyISAM won't cut it.

Other systems
--------------

Currently unsupported.

Upgrading
=========

You can use [South](http://south.aeracode.org/) for database migrations.

Usage
=====

Just like any other Django app.

Links
=====

GSoC 2011 Project:  http://www.google-melange.com/gsoc/project/google/gsoc2011/vh4x0r/26001  
GSoC 2011 Proposal: http://www.google-melange.com/gsoc/proposal/review/google/gsoc2011/vh4x0r/1  
GSoC 2012 Project:  http://www.google-melange.com/gsoc/project/google/gsoc2012/gg7/28001  
