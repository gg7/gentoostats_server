import os
import time
import pickle
import logging

from django.core.exceptions import ValidationError

logger = logging.getLogger(__name__)

def validate_item(item):
    """
    Call .full_clean() on a Django model object.

    If this results in a ValidationError, then a HttpResponseBadRequest is
    raised.
    """

    # TODO: also log information such as type(item).

    try:
        item.full_clean()
    except ValidationError:
        error_message = "Error: '%s' failed validation." % str(item)
        logger.info("validate_item(): " + error_message, exc_info=True)
        raise ValidationError(error_message)


def create_new_objs(cls, all_objs):
    existing_objs = cls.objects\
                       .filter(name__in=all_objs)\
                       .values_list("name", flat=True)

    new_objs = [cls(name=x) for x in
        set(all_objs).difference(existing_objs)
    ]

    map(validate_item, new_objs)
    cls.objects.bulk_create(new_objs)

    result = cls.objects.filter(name__in=all_objs)
    assert len(result) == len(all_objs)
    return result


def list_to_dict(lst):
    """Converts a list() to a dict() using each element's .name attribute."""
    return dict([(obj.name, obj) for obj in lst])

