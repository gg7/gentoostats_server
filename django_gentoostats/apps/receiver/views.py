import json
import time
import logging
import itertools
from datetime import datetime

from django.db import IntegrityError, transaction
from django.http import HttpResponse, HttpResponseBadRequest, \
                        HttpResponseForbidden, HttpResponseServerError
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.utils.timezone import utc
from django.core.exceptions import ValidationError
from django.contrib.gis.geoip import GeoIP
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

import pytz

import dateutil

import jsonschema

from rest_framework import status

from portage.dep import Atom as PortageAtom
from portage.exception import InvalidAtom
from portage._sets import SETPREFIX as SET_PREFIX

from .util import validate_item, create_new_objs, list_to_dict
from .forms import UploadForm
from .json_schema import schema3
from ..stats.util import canonicalize_uuid
from ..stats.models import Category, PackageName, Repository, Package, Atom, \
                    UseFlag, Lang, Host, Feature, MirrorServer, SyncServer, \
                    Keyword, Installation, AtomSet, Submission

logger = logging.getLogger(__name__)

CURRENT_PROTOCOL_VERSION = 3

# TODO: trasncation control
def process_submission(request, undecoded_json):
    """
    Parses and saves a submission.
    """

    # Decode the JSON:
    try:
        data = json.loads(undecoded_json)
    except Exception:
        return (status.HTTP_400_BAD_REQUEST, "Error: Unable to decode JSON data.")

    # Do we support this protocol version?
    protocol = data.get('PROTOCOL')
    if protocol is None:
        return (status.HTTP_400_BAD_REQUEST, "Error: No protocol version specified.")
    elif type(protocol) != int:
        return (status.HTTP_400_BAD_REQUEST, "Error: The protocol version must be an integer.")
    elif protocol != CURRENT_PROTOCOL_VERSION:
        return (status.HTTP_400_BAD_REQUEST, "Error: Your current client is unsupported, please update.")

    try:
        jsonschema.validate(data, schema3)
    except jsonschema.ValidationError as e:
        logger.info("process_submission(): JSON validation error is " + str(e), exc_info=True)
        return (status.HTTP_400_BAD_REQUEST, "Error: JSON schema validation failed: %s (%s)"  % (e.message, "/".join(e.schema_path)))

    # UUIDs are to be saved in the DB in lower case and hyphenated.
    uuid       = canonicalize_uuid(data['AUTH']['UUID'])
    upload_key = data['AUTH']['PASSWD']

    # return (status.HTTP_200_OK, "Success")
    # return (status.HTTP_500_INTERNAL_SERVER_ERROR, "Features length mismatch")

    try:
        host, _ = Host.objects.get_or_create(id=uuid, upload_key=upload_key)
        host.full_clean()
    except IntegrityError:
        return (status.HTTP_403_FORBIDDEN, "Error: Invalid password.")
    except ValidationError:
        return (status.HTTP_400_BAD_REQUEST, "Error: Invalid AUTH values. Is your password too long?")

    tree_age = data.get('TREEAGE')
    if tree_age:
        try:
            # In Python 2.7 time.strptime() does not always support %z'.
            # More info: http://bugs.python.org/issue6641 .
            # That's why I'm using dateutil here.
            tree_age = dateutil.parser.parse(tree_age).astimezone(pytz.utc)
        except ValueError:
            return (status.HTTP_400_BAD_REQUEST, "Error: Invalid tree age.")

    features = data.get('FEATURES')
    if features:
        features = create_new_objs(Feature, features)

    use_flags = data.get('USE')
    if use_flags:
        use_flags = create_new_objs(UseFlag, use_flags)

    keywords = data.get('ACCEPT_KEYWORDS')
    if keywords:
        keywords = [
            Keyword.objects.get_or_create(name=k)[0] for k in keywords
        ]

        map(validate_item, keywords)

    mirrors = data.get('GENTOO_MIRRORS')
    if mirrors:
        mirrors = [
            MirrorServer.objects.get_or_create(url=m)[0] for m in mirrors
        ]

        map(validate_item, mirrors)

    # return (status.HTTP_200_OK, "Success")

    lang = data.get('LANG')
    if lang:
        lang, _ = Lang.objects.get_or_create(name=lang)
        validate_item(lang)

    sync = data.get('SYNC')
    if sync:
        sync, _ = SyncServer.objects.get_or_create(url=sync)
        validate_item(sync)

    ip_addr  = request.META['REMOTE_ADDR']
    fwd_addr = request.META.get('HTTP_X_FORWARDED_FOR') # TODO

    public = data.get('PUBLIC') == True

    submission = Submission.objects.create(
        host          = host,
        country       = GeoIP().country_name(ip_addr),
        raw_data      = data,
        protocol      = protocol,
        ip_addr       = ip_addr,
        fwd_addr      = fwd_addr,
        email         = data['AUTH'].get('EMAIL'),
        public        = public,

        arch          = data.get('ARCH'),
        chost         = data.get('CHOST'),
        cbuild        = data.get('CBUILD'),
        ctarget       = data.get('CTARGET'),

        platform      = data.get('PLATFORM'),
        profile       = data.get('PROFILE'),
        makeconf      = data.get('MAKECONF'),

        cflags        = data.get('CFLAGS'),
        cxxflags      = data.get('CXXFLAGS'),
        ldflags       = data.get('LDFLAGS'),
        fflags        = data.get('FFLAGS'),

        makeopts      = data.get('MAKEOPTS'),
        emergeopts    = data.get('EMERGE_DEFAULT_OPTS'),
        syncopts      = data.get('PORTAGE_RSYNC_EXTRA_OPTS'),
        acceptlicense = data.get('ACCEPT_LICENSE'),

        lang          = lang,
        sync          = sync,

        tree_age      = tree_age,
    )

    submission.features.add(*features)
    submission.mirrors.add(*mirrors)

    submission.global_use.add(*use_flags)
    submission.global_keywords.add(*keywords)

    packages = data.get('PACKAGES')
    if packages:
        num_created_installations = 0
        num_created_packages = 0

        all_use_flags_set = set()

        for _, package_info in packages.items():
            all_use_flags_set.update( package_info.get('IUSE'),
                                      package_info.get('PKGUSE'),
                                      package_info.get('USE'),
            )

        existing_use_flags_as_str = UseFlag.objects\
                                           .filter(name__in=all_use_flags_set)\
                                           .values_list('name', flat=True)

        new_use_flags = [UseFlag(name=u) for u in \
                all_use_flags_set.difference(existing_use_flags_as_str)
        ]

        map(validate_item, new_use_flags)

        UseFlag.objects.bulk_create(new_use_flags)

        # Great, we have created all the new use flags. You can surely use
        # objects from 'new_use_flags' in 'installation.iuse.add()', right?
        # See what this 'helpful' error message has to say to you:
        #   "instance is on database "default", value is on database "None"
        # https://github.com/django/django/blob/18ffdb1772ba60e085cff8fd9a1d4a7b129b4032/django/db/models/fields/related.py#L611
        # It appears that we are forced to create new UseFlag objects for the
        # new use flags. Maybe one day this will be fixed too:
        # https://code.djangoproject.com/ticket/19527

        all_use_flags = UseFlag.objects.filter(name__in=all_use_flags_set);
        assert len(all_use_flags_set) == len(all_use_flags)

        use_flags_dict = list_to_dict(all_use_flags)

        all_categories    = set()
        all_package_names = set()
        all_repos         = set()
        all_packages      = set()
        all_keywords      = set()

        for package, package_info in packages.items():
            atom = PortageAtom( "=" + package
                              , allow_wildcard = False
                              , allow_repo     = False # ::repo is never sent by the client
            )

            category, package_name = atom.cp.split('/')
            all_categories.add(category)
            all_package_names.add(package_name)

            repo = package_info.get('REPO')
            if repo:
                all_repos.add(repo)

            keyword = package_info.get('KEYWORD')
            if keyword:
                all_keywords.add(keyword)

        ###

        # 20985

        categories    = create_new_objs(Category, all_categories)
        package_names = create_new_objs(PackageName, all_package_names)
        repos         = create_new_objs(Repository, all_repos)
        keywords      = create_new_objs(Keyword, all_keywords)

        categories_dict    = list_to_dict(categories)
        package_names_dict = list_to_dict(package_names)
        repos_dict         = list_to_dict(repos)
        keywords_dict      = list_to_dict(keywords)

        ###

        # Package = todo
















        for package, package_info in packages.items():
            try:
                atom = PortageAtom( "=" + package
                                  , allow_wildcard = False
                                  , allow_repo     = False
                )

                category, package_name = atom.cp.split('/')

                category     = categories_dict[category]
                package_name = package_names_dict[package_name]

                # TODO: decided whether to support atom.repo at all
                repo = package_info.get('REPO')
                if repo:
                    repo = repos_dict[repo]

                package, created = Package.objects.get_or_create(
                    category     = category,
                    package_name = package_name,

                    version      = atom.cpv.lstrip(atom.cp),
                    slot         = atom.slot,
                    repository   = repo,
                )
                if created:
                    package.full_clean()
                    num_created_packages += 1

            except (InvalidAtom, ValidationError):
                return (status.HTTP_400_BAD_REQUEST, "Error: Atom '%s' failed validation." % package)

            keyword = package_info.get('KEYWORD')
            if keyword:
                keyword = keywords_dict[keyword]

            built_at = package_info.get('BUILD_TIME')
            if not built_at:
                # Sometimes clients report BUILD_TIME as '', so we convert
                # build_at to None so that this is reflected in the database.
                built_at = None
            else:
                built_at = datetime.utcfromtimestamp(float(built_at))
                # built_at = built_at.replace(tzinfo=utc)

            build_duration = package_info.get('BUILD_DURATION')
            if not build_duration:
                # Convert '' into None
                build_duration = None

            size = package_info.get('SIZE')
            if not size:
                # Convert '' into None
                size = None

            installation, created = Installation.objects.get_or_create(
                package = package,
                keyword = keyword,

                built_at       = built_at,
                build_duration = build_duration, # TODO
                size           = size,
            )

            iuse   = [use_flags_dict[u] for u in package_info.get('IUSE')]
            pkguse = [use_flags_dict[u] for u in package_info.get('PKGUSE')]
            use    = [use_flags_dict[u] for u in package_info.get('USE')]

            if iuse:
                installation.iuse.add(*iuse)
            if pkguse:
                installation.pkguse.add(*pkguse)
            if use:
                installation.use.add(*use)

            if created:
                num_created_installations += 1
                installation.full_clean()

            submission.installations.add(installation)

        logger.debug("process_submission(): %s created installations" % num_created_installations)
        logger.debug("process_submission(): %s created packages" % num_created_packages)

    reported_sets = data.get('WORLDSET')
    if reported_sets:
        for set_name, entries in reported_sets.items():
            try:
                atom_set, _ = AtomSet.objects.get_or_create(
                    name  = set_name,
                    owner = submission,
                )

                for entry in entries:
                    try:
                        if entry.startswith(SET_PREFIX):
                            subset_name = entry[len(SET_PREFIX):]

                            subset, _ = AtomSet.objects.get_or_create(
                                name  = subset_name,
                                owner = submission,
                            )
                            subset.full_clean()

                            atom_set.subsets.add(subset)
                        else:
                            patom = PortageAtom( entry
                                               , allow_wildcard = False
                                               , allow_repo     = True
                            )

                            category, package_name = patom.cp.split('/')

                            category, _ = Category.objects.get_or_create(name=category)
                            category.full_clean()

                            package_name, _ = PackageName.objects.get_or_create(name=package_name)
                            package_name.full_clean()

                            repo = patom.repo
                            if repo:
                                repo, created = Repository.objects.get_or_create(name=repo)
                                if created:
                                    repo.full_clean()

                            atom, _ = Atom.objects.get_or_create(
                                full_atom    = entry,
                                operator     = patom.operator or '',

                                category     = category,
                                package_name = package_name,
                                version      = patom.cpv.lstrip(patom.cp),
                                slot         = patom.slot,
                                repository   = repo,
                            )

                            atom.full_clean()
                            atom_set.atoms.add(atom)
                    except (InvalidAtom, ValidationError):
                        return (status.HTTP_400_BAD_REQUEST, "Error: Atom/set '%s' failed validation." % entry)

                atom_set.full_clean()
                submission.reported_sets.add(atom_set)
            except ValidationError:
                return (status.HTTP_400_BAD_REQUEST, "Error: Selected set '%s' failed validation." % set_name)

    submission.full_clean()
    return (status.HTTP_200_OK, "Success")

@csrf_exempt
@transaction.commit_on_success
def accept_submission(request):
    """
    Simple wrapper around process_submission().
    """

    form = UploadForm()
    if request.method == 'POST':
        try:
            code, message = process_submission(request, request.FILES['file_'].read())
            # code, message = process_submission(request, request.body)

            # TODO: Use a switch statement (dict) here:
            if code == status.HTTP_200_OK:
                return render_to_response('receiver/upload.html', {'form': form}, context_instance=RequestContext(request))
                # return HttpResponse("Success", content_type="text/plain")
            elif code == status.HTTP_400_BAD_REQUEST:
                return HttpResponseBadRequest(message, content_type="text/plain")
            elif code == status.HTTP_403_FORBIDDEN:
                return HttpResponseForbidden(message, content_type="text/plain")
            else:
                return HttpResponseServerError("TODO", content_type="text/plain")

        except Exception as e:
            logger.error("process_submission(): " + str(e), exc_info=True)
            r = "Error: something went wrong. The administrator has been " + \
                "notified and will look into the problem."
            return HttpResponseServerError(r, content_type="text/plain")
    else:
        return render_to_response('receiver/upload.html', {'form': form}, context_instance=RequestContext(request))
