from django import forms

class UploadForm(forms.Form):
    file_ = forms.FileField()
