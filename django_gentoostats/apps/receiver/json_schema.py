# Remember that JSON uses double quotes (") for quoting values, not single ones
# (').

schema2 = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "Submission",
    "type": "object",
    "properties": {
        "ACCEPT_KEYWORDS": {
            "type": "array",
            "items": {
                "type": "string",
            },
            "minItems": 1,
            "uniqueItems": True,
            "description": "Example: ['amd64', '~amd64']",
        },

        "ACCEPT_LICENSE": {
            "type": "string",
            "description": "Example: '* -@EULA'",
        },

        "ARCH": {
            "type": "string",
            "description": "Example: 'amd64'",
        },

        "AUTH": {
            "type": "object",
            "properties": {
                "PASSWD": {"type": "string"},
                "UUID":  {"type": "string"},
            },
            "required": ["PASSWD", "UUID"]
        },

        "CFLAGS": {
            "type": "string",
            "description": "Example: '-march=native -O2 -pipe'",
        },

        "CHOST": {
            "type": "string",
            "description": "Example: 'x86_64-pc-linux-gnu'",
        },

        "CTARGET": {
            "type": "string",
            "description": "Example: TODO",
        },

        "CXXFLAGS": {
            "type": "string",
            "description": "Example: '-march=native -O2 -pipe'",
        },

        "EMERGE_DEFAULT_OPTS": {
            "type": "string",
            "description": "Example: '--complete-graph=y --jobs=2 --load-average=1.85'",
        },

        "FEATURES": {
            "type": "array",
            "items": {
                "type": "string",
            },
            "minItems": 0,
            "uniqueItems": True,
            "description": "Example: ['parallel-fetch', 'preserve-libs']",
        },

        "FFLAGS": {
            "type": "string",
            "description": "Example: '-O2 -pipe'",
        },

        "GENTOO_MIRRORS": {
            "type": "array",
            "items": {
                "type": "string",
            },
            "minItems": 0,
            "uniqueItems": True,
            "description": "Example: ['http://gentoo.osuosl.org/']",
        },

        "LANG": {
            "type": "string",
            "description": "Example: 'en_US.utf8'",
        },

        "LASTSYNC": {
            "type": "string",
            "description": "Example: 'Tue, 24 Sep 2013 14:30:01 +0000'",
        },

        "LDFLAGS": {
            "type": "string",
            "description": "Example: '-Wl,-O1 -Wl,--as-needed'",
        },

        "MAKEOPTS": {
            "type": "string",
            "description": "Example: '-j2 -l1.95'",
        },

        "PACKAGES": {
            "type": "object", # TODO
        },

        "PLATFORM": {
            "type": "string",
            "description": "Example: 'Linux-3.2.46-gentoo-x86_64-Intel-R-_Core-TM-_i3_CPU_M_330_@_2.13GHz-with-gentoo-2.2'",
        },

        "PORTAGE_RSYNC_EXTRA_OPTS": {
            "type": "string",
            "description": "Example: '-q'",
        },

        "PROFILE": {
            "type": "string",
            "description": "Example: 'default/linux/amd64/13.0/desktop/kde'",
        },

        "PROTOCOL": {
            "type": "integer",
            "description": "Protocol version. Example: 2",
        },

        "PUBLIC": {
            "type": "string",
            "description": "TODO",
        },

        "SYNC": {
            "type": "string",
            "description": "Example: 'rsync://rsync.gentoo.org/gentoo-portage'",
        },

        "USE": {
            "type": "array",
            "items": {
                "type": "string",
                # "pattern": "TODO",
            },
            "minItems": 0,
            "uniqueItems": True,
            "description": "Example: ['X', 'alsa']",
        },

        "WORLDSET": {
            "type": "object",
        },
    },

    "additionalProperties": False,
    "required": ["AUTH", "PROTOCOL"],
}

schema3 = schema2
schema3['properties']['TREEAGE'] = schema2['properties']['LASTSYNC']
del schema2['properties']['LASTSYNC']
