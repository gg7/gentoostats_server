from __future__ import division

from django import template
from django.utils.html import escape
from django.utils.safestring import mark_safe

from ..util import reduce_flag

register = template.Library()

@register.filter()
def split(lst):
    """
    >>> split([1, 2, 3, 4])
    (1, [2, 3, 4])
    """
    return lst[0], lst[1:]

@register.filter()
def second(lst):
    return lst[1]

@register.filter()
def index(lst, i):
    """
    >>> index(['a', 'b', 'c'], 0)
    'a'
    >>> index(['a', 'b', 'c'], -1)
    'c'
    """
    return lst[i]

@register.filter()
def tail(lst):
    """Short for |slice:'1:'
    >>> tail([1, 2])
    [2]
    >>> tail([1])
    []
    >>> tail([])
    []
    """
    return lst[1:]

@register.filter()
def divide(a, b):
    """TODO"""
    return round(100*a/b)

@register.filter()
def format_use_flags(installation):
    """Prints USE flag information with <span> elements."""

    use    = [x.name for x in list(installation.use.all())]
    pkguse = [x.name for x in list(installation.pkguse.all())]

    # Remove '-' and '+' from the available package useflags:
    iuse   = [reduce_flag(x.name) for x in list(installation.iuse.all())]

    result = []
    for f in iuse:
        if f in pkguse:
            if f in use:
                # Selected & Enabled:
                use_class = 'use-selected'
            else:
                # Selected but not enabled:
                use_class = 'use-invalid'
        elif f in use:
            # Not Selected but enabled:
            use_class = 'use-enabled'
        else:
            # Add a '-' to help colour-impaired users:
            f = '-' + f

            if f in pkguse:
                # Manually disabled:
                use_class = 'use-unselected'
            else:
                # Simply disabled:
                use_class = 'use-disabled'

        result.append(
            '<span class="use %s">%s</span>' % (use_class, escape(f))
        )

    return mark_safe(" ".join(result))
