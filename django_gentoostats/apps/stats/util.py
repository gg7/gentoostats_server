import os
import logging

logger = logging.getLogger(__name__)

def split_list(lst):
    """Split a list into head:tail."""

    return lst[0], lst[1:]


def canonicalize_uuid(uuid, hyphens=True):
    """
    Lowercases and optionally hyphenates the UUID.

    UUIDv4 format: xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx
            where: x = any hexadecimal digit
                   y = one of [8, 9, a, b]

    Raises an exception if the UUID is of invalid length.
    """

    uuid_no_hyphens = uuid.replace('-', '')

    if len(uuid_no_hyphens) != 32:
        raise Exception("Invalid UUID length")

    if hyphens:
        format_string = "%s-%s-%s-%s-%s"
    else:
        format_string = "%s%s%s%s%s"

    return format_string % ( uuid_no_hyphens[:8]
                           , uuid_no_hyphens[8:12]
                           , uuid_no_hyphens[12:16]
                           , uuid_no_hyphens[16:20]
                           , uuid_no_hyphens[20:]
    )


def percentify(n, num_hosts):
    """
    >>> percentify(3, 5)
    60
    """

    # TODO: remove this hack
    settings_module = os.environ['DJANGO_SETTINGS_MODULE']
    DEBUG = __import__(settings_module, fromlist=["DEBUG"]).DEBUG
    if DEBUG:
        import random
        return random.randint(0, 100)
    else:
        return round(n * 100 / num_hosts)


# The following code is based on code from gentoolkit.flag, I've copied it here
# for performance (importing it from gentoolkit is slow).
def reduce_flag(flag):
    """Absolute value function for a USE flag.

    @type flag: string
    @param flag: USE flag
    @rtype: string
    @return absolute USE flag

    >>> map(reduce_flag, ['-doc', 'doc', '+doc', ' +doc'])
    ['doc', 'doc', 'doc', ' +doc']
    """

    if flag[0] in ["+", "-"]:
        return flag[1:]
    return flag
