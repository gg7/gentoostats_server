from __future__ import division

import json
import operator
import datetime

from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import cache_page, cache_control
from django.core.exceptions import ObjectDoesNotExist
from django.utils.timezone import utc
from django.views.generic import ListView, DetailView
from django.db.models import Q, Count
from django.shortcuts import render, redirect, get_object_or_404
from django.http import Http404

from portage.exception import InvalidAtom
from portage.dep import Atom as PortageAtom

from .util import split_list, canonicalize_uuid, percentify
from .forms import HostSearchForm
from .models import Feature, Host, Keyword, Lang, MirrorServer, Package, \
                    Repository, Submission, SyncServer, UseFlag

FRESH_SUBMISSION_MAX_AGE = 30 # in days

@cache_control(public=True)
@cache_page(1 * 60)
def index(request):
    """
    Index page of Gentoostats.
    """

    return render(request, 'stats/index.html')

@cache_control(public=True)
@cache_page(24 * 60 * 60)
def faq(request):
    """
    Frequently Asked Questions page.
    """

    return render(request, 'stats/not_implemented.html')

@cache_control(public=True)
@cache_page(24 * 60 * 60)
def about(request):
    """
    Project about page.
    """

    return render(request, 'stats/about.html')

@cache_control(public=True)
@cache_page(24 * 60 * 60)
def stats(request):
    """
    Stats index page.
    """

    return render(request, 'stats/stats_index.html')

@cache_control(public=True)
@cache_page(1 * 60)
def overall_stats(request):
    """
    Show overall stats for the website.
    """

    latest_submission_q = Q(submissions__in=Submission.objects.latest_submission_ids)

    features = Feature.objects.filter(latest_submission_q).annotate(num_hosts_fast=Count('submissions')).order_by('name')
    langs    = Lang.objects.filter(latest_submission_q).annotate(num_hosts_fast=Count('submissions')).order_by('name')
    keywords = Keyword.objects.filter(latest_submission_q).annotate(num_hosts_fast=Count('submissions')).order_by('name')

    context = dict(
        hosts         = Host.objects.all(),
        submissions   = Submission.objects.all(),
        country_stats = Submission.objects.latest_submissions_unique.filter(country__isnull=False).values_list('country').annotate(num_hosts=Count('country')).order_by('country'),
        arch_stats    = Submission.objects.latest_submissions_unique.values_list('arch').annotate(num_hosts=Count('arch')).order_by('arch'),
        profile_stats = Submission.objects.latest_submissions_unique.values_list('profile').annotate(num_hosts=Count('profile')).order_by('profile'),
        useflags      = UseFlag.objects.all(),
        packages      = Package.objects.all(),
        features      = features,
        langs         = langs,
        keywords      = keywords,
        latest_public_submissions_all = Submission.objects.latest_public_submissions_all,
    )

    return render(request, 'stats/overall_stats.html', context)

# Hosts: #{{{
@cache_control(public=True)
@cache_page(24 * 60 * 60)
def host_search(request):
    """
    Search for a host by its full UUID.
    """

    if request.method == 'POST':
        form = HostSearchForm(request.POST)
        if form.is_valid():
            return redirect( 'stats:host_details_url'
                           , host_id   = form.cleaned_data['host_id']
                           , permanent = False
            )
    else:
        form = HostSearchForm(
            # initial = dict(host_id='Enter UUID'),
        )

    context = dict(
        form=form,
    )

    return render(request, 'stats/host_search.html', context)

@cache_control(private=True)
@cache_page(1 * 60)
def host_details(request, host_id):
    """
    Show statistics about a certain host.
    """

    # Accept UUIDs regardless of hyphenation or case sensitivity, but still
    # redirect the user to their canonical URL:
    host_id_canonical = canonicalize_uuid(host_id)

    if host_id != host_id_canonical:
        return redirect( 'stats:host_details_url'
                       , host_id   = host_id_canonical
                       , permanent = True
        )

    context = dict(
        host = get_object_or_404( Host.objects.select_related('submissions')
                                , id = host_id
        ),
    )

    return render(request, 'stats/host_details.html', context)
#}}}

@cache_control(public=True)
@cache_page(1 * 60)
def arch_details(request, arch):
    """
    Show more detailed statistics about a specific arch.
    """

    try:
        context = dict(
            stats_type      = "Arch",
            value           = arch,
            num_submissions = Submission.objects.filter(arch=arch).count(),
            num_all_hosts   = Submission.objects.filter(arch=arch).order_by().aggregate(Count('host', distinct=True)).values()[0],
            num_hosts       = Submission.objects.latest_submissions_unique.filter(arch=arch).count(),
            added_on        = Submission.objects.filter(arch=arch).values('added_on').order_by('added_on')[:1].get().added_on,
        )
    except ObjectDoesNotExist:
        raise Http404

    return render(request, 'stats/generic_details.html', context)

@cache_control(public=True)
@cache_page(1 * 60)
def keyword_details(request, keyword):
    """
    Show statistics about a particular keyword (e.g. amd64).
    """

    context = dict(
        keyword = get_object_or_404(Keyword, name=keyword),
    )

    return render(request, 'stats/keyword_details.html', context)

@cache_control(public=True)
@cache_page(1 * 60)
def feature_details(request, feature):
    """
    Show statistics about a particular FEATURE.
    """

    context = dict(
        feature = get_object_or_404(Feature, name=feature),
    )

    return render(request, 'stats/feature_details.html', context)

# Servers: #{{{
@cache_control(public=True)
@cache_page(1 * 60)
def server_stats(request):
    """
    Show statistics about the known sync and mirror servers.
    """

    context = dict(
        mirror_servers = MirrorServer.objects.all(),
        sync_servers   = SyncServer.objects.all(),
    )

    return render(request, 'stats/server_stats.html', context)

@cache_control(public=True)
@cache_page(1 * 60)
def mirror_details(request, server_id):
    """
    Show more detailed statistics about a specific mirror server.
    """

    context = dict(
        server = get_object_or_404(MirrorServer, id=server_id)
    )

    return render(request, 'stats/server_details.html', context)

@cache_control(public=True)
@cache_page(1 * 60)
def sync_details(request, server_id):
    """
    Show more detailed statistics about a specific SYNC server.
    """

    context = dict(
        server = get_object_or_404(MirrorServer, id=server_id),
    )

    return render(request, 'stats/server_details.html', context)
#}}}

# Repositories: #{{{
@cache_control(public=True)
@cache_page(1 * 60)
def repository_stats(request):
    """
    TODO: add a description.
    """

    context = dict(
        repositories = Repository.objects.all(),
    )

    return render(request, 'stats/repository_stats.html', context)

@cache_control(public=True)
@cache_page(1 * 60)
def repository_details(request, name):
    """
    TODO: add a description.
    """

    repo = get_object_or_404(Repository, name=name)

    context = dict(
        stats_type      = "Repository",
        value           = repo,
        num_submissions = repo.num_submissions,
        num_all_hosts   = repo.num_all_hosts,
        num_hosts       = repo.num_hosts,
        added_on        = repo.added_on,
    )

    return render(request, 'stats/generic_details.html', context)
#}}}

@cache_control(public=True)
@cache_page(1 * 60)
def package_search(request):
    """
    TODO: Use a real form here.
    """

    return render(request, 'stats/package_search.html')

@cache_control(public=True)
@cache_page(1 * 60)
def package_details(request, package):
    """
    TODO: add a description.
    """

    context = dict(
        query = package,
    )

    try:
        patom = PortageAtom( package
                           , allow_wildcard = False # at least for now
                           , allow_repo     = True
        )
    except InvalidAtom:
        return render( request
                     , 'stats/package_details.html' # reuse the same template
                     , dict(context, invalid=True)  # set context['invalid']
                     , status=400                   # "Bad Request"
        )

    # Find all Package objects that satisfy the PortageAtom by using Q objects:
    q_list = [Q(cp=patom.cp)]

    if patom.repo:
        q_list.append(Q(repository__name=patom.repo))

    if patom.version:
        q_list.append(Q(version=patom.version))

    if patom.slot:
        q_list.append(Q(slot=patom.slot))

    q_final = reduce(operator.and_, q_list)
    matching_packages = Package.objects.filter(q_final).distinct()

    context['packages'] = matching_packages
    return render(request, 'stats/package_details.html', context)

@cache_control(public=True)
@cache_page(1 * 60)
def lang_details(request, lang):
    """
    TODO: add a description.
    """

    return render(request, 'stats/not_implemented.html')

# Submissions: #{{{
# TODO: @login_required(login_url='/login')
@cache_control(private=True)
@cache_page(24 * 60 * 60)
def submission_details(request, host_id, relative_id):
    """relative_id is the submission number (starting from 1) relative to the
    host"""
    relative_id = int(relative_id)
    if relative_id < 0:
        raise Http404

    submission_id = Submission.objects\
            .filter(host=host_id)\
            .values_list('id', flat=True)\
            .order_by('id')[relative_id-1:relative_id]\
            .get()

    # Prefetch everything required so that the template doesn't have to hit the
    # database. The resulting SQL isn't very pretty, but it works.
    s = Submission.objects.select_related( 'host'
                                         , 'arch'
                                         , 'chost'
                                         , 'cbuild'
                                         , 'ctarget'
                                         , 'platform'
                                         , 'profile'
                                         , 'lang'
                                         , 'lastsync'
                                         , 'makeconf'
                                         , 'cflags'
                                         , 'cxxflags'
                                         , 'ldflags'
                                         , 'fflags'
                                         , 'sync'
                                         , 'makeopts'
                                         , 'emergeopts'
                                         , 'syncopts'
                                         , 'acceptlicense')\
            .prefetch_related('features')\
            .prefetch_related('mirrors')\
            .prefetch_related('global_use')\
            .prefetch_related('global_keywords')\
            .prefetch_related('installations')\
            .prefetch_related('installations__package')\
            .prefetch_related('installations__package__category')\
            .prefetch_related('installations__package__package_name')\
            .prefetch_related('installations__package__repository')\
            .prefetch_related('installations__keyword')\
            .prefetch_related('installations__iuse')\
            .prefetch_related('installations__pkguse')\
            .prefetch_related('installations__use')\
            .prefetch_related('reported_sets')\
            .prefetch_related('reported_sets__atoms')\
            .prefetch_related('reported_sets__subsets')\
            .get(id=submission_id)

    context = dict(
        submission = s
    )
    return render(request, 'stats/submission_detail.html', context)


# USE Flags: #{{{
@cache_control(public=True)
@cache_page(1 * 60)
def use_stats(request):
    """
    Global USE flag stats.
    """

    latest_submission_q = Q(submissions__in=Submission.objects.latest_submission_ids)
    use_stats = UseFlag.objects.filter(latest_submission_q).annotate(num_hosts_fast=Count('submissions')).order_by('name')

    context = dict(
        use_stats = use_stats,
    )

    return render(request, 'stats/use_stats.html', context)

@cache_control(public=True)
@cache_page(1 * 60)
def use_details(request, useflag):
    """
    Detailed USE flag stats.
    """

    useflag = get_object_or_404(UseFlag, name=useflag)

    context = dict(
        stats_type      = "USE Flag",
        value           = useflag,
        num_submissions = useflag.num_submissions,
        num_all_hosts   = useflag.num_all_hosts,
        num_hosts       = useflag.num_hosts,
        added_on        = useflag.added_on,
    )

    return render(request, 'stats/generic_details.html', context)
#}}}

@cache_control(public=True)
@cache_page(1 * 60)
def profile_details(request, profile):
    """
    Detailed profile stats.
    """

    try:
        context = dict(
            stats_type      = "Profile",
            value           = profile,
            num_submissions = Submission.objects.filter(profile=profile).count(),
            num_all_hosts   = Submission.objects.filter(profile=profile).order_by().aggregate(Count('host', distinct=True)).values()[0],
            num_hosts       = Submission.objects.latest_submissions_unique.filter(profile=profile).count(),
            added_on        = Submission.objects.filter(profile=profile).values('added_on').order_by('added_on')[:1].get().added_on,
        )
    except ObjectDoesNotExist:
        raise Http404

    return render(request, 'stats/generic_details.html', context)

@cache_control(public=True)
#@cache_page(1 * 60) # TODO: uncomment this in production
@cache_page(0)
def app_stats(request):
    stats = [
        [ 'Browsers'
        ,   ['Chrome', 'www-client/google-chrome', 'www-client/chromium']
        ,   ['Firefox', 'www-client/firefox', 'www-client/firefox-bin']
        ,   ['Opera', 'www-client/opera']
        ,   ['Epiphany', 'www-client/epiphany']
        ,   ['Konqueror', 'kde-base/konqueror']
        ,   ['rekonq', 'www-client/rekonq']
        ,   ['Conkeror', 'www-client/conkeror']
        ,   ['Midori', 'www-client/midori']
        ,   ['Uzbl', 'www-client/uzbl']
        ],

        [ 'CLI Browsers'
        ,   ['Wget', 'net-misc/wget']
        ,   ['cURL', 'net-misc/curl']
        ,   ['Lynx', 'www-client/lynx']
        ,   ['Links', 'www-client/links']
        ,   ['ELinks', 'www-client/elinks']
        ,   ['W3M', 'www-client/w3m', 'www-client/w3mmee']
        ],

        [ 'Editors/IDEs'
        ,   ['Vi/Vim', 'app-editors/vim', 'app-editors/gvim', 'app-editors/nvi', 'app-editors/elvis']
        ,   ['Emacs', 'app-editors/emacs', 'app-editors/qemacs', 'app-editors/xemacs', 'app-editors/jove']
        ,   ['Eclipse', 'dev-util/eclipse-sdk']
        ,   ['Yi', 'app-editors/yi']
        ,   ['Nano', 'app-editors/nano']
        ,   ['Gedit', 'app-editors/gedit']
        ,   ['Kate', 'kde-base/kate']
        ,   ['Kwrite', 'kde-base/kwrite']
        ,   ['Ne', 'app-editors/ne']
        ,   ['Jed', 'app-editors/jed']
        ,   ['Jedit', 'app-editors/jedit']
        ,   ['Joe', 'app-editors/joe']
        ,   ['Ed', 'sys-apps/ed']
        ,   ['Leafpad', 'app-editors/leafpad']
        ,   ['Geany', 'dev-util/geany']
        ],

        [ 'Desktop Environments'
        ,   ['KDE SC', 'kde-base/kdebase-meta']
        ,   ['GNOME', 'gnome-base/gnome']
        ,   ['Xfce', 'xfce-base/xfce4-meta']
        ,   ['LXDE', 'lxde-base/lxde-meta']
        #,   ['E17', 'dev-libs/ecore']
        ],

        [ 'Window Managers'
        ,   ['Xmonad', 'x11-wm/xmonad']
        ,   ['Ratpoison', 'x11-wm/ratpoison']
        ,   ['Openbox', 'x11-wm/openbox']
        ,   ['Fluxbox', 'x11-wm/fluxbox']
        ,   ['Enlightenment', 'x11-wm/enlightenment']
        ,   ['dwm', 'x11-wm/dwm']
        ,   ['i3', 'x11-wm/i3']
        ,   ['Compiz', 'x11-wm/compiz', 'x11-wm/compiz-fusion']
        ,   ['FVWM', 'x11-wm/fvwm']
        ,   ['Wmii', 'x11-wm/wmii']
        ,   ['Window Maker', 'x11-wm/windowmaker']
        ,   ['subtle', 'x11-wm/subtle']
        ,   ['awesome', 'x11-wm/awesome']
        ,   ['evilwm', 'x11-wm/evilwm']
        ,   ['IceWM', 'x11-wm/icewm']
        ],

        [ 'Shells'
        ,   ['Bash', 'app-shells/bash']
        ,   ['Zsh', 'app-shells/zsh']
        ,   ['Tcsh', 'app-shells/tcsh']
        ,   ['fish', 'app-shells/fish']
        ],

        [ 'Web servers'
        ,   ['Apache', 'www-servers/apache']
        ,   ['Nginx', 'www-servers/nginx']
        ,   ['lighttpd', 'www-servers/lighttpd']
        ],

        [ 'Graphics Drivers'
        ,   ['Nvidia (proprietary)', 'x11-drivers/nvidia-drivers']
        ,   ['Nouveau', 'x11-drivers/xf86-video-nouveau']
        ,   ['fglrx (proprietary)', 'x11-drivers/ati-drivers']
        ,   ['radeon', 'x11-drivers/xf86-video-ati']
        ,   ['Intel', 'x11-drivers/xf86-video-intel']
        ],
    ]

    delta = datetime.timedelta(days=FRESH_SUBMISSION_MAX_AGE)
    submission_age = datetime.datetime.utcnow().replace(tzinfo=utc) - delta

    fresh_submissions_qs = Submission.objects\
                                     .latest_submissions_unique\
                                     .filter(added_on__gte=submission_age)

            # TODO:
            # .prefetch_related('installations')\
            # .prefetch_related('installations__package')\
            # .prefetch_related('installations__package__cp')
    num_hosts = fresh_submissions_qs.count()

    for section in stats:
        cat, apps = split_list(section)
        for app in apps:
            # Turn this: ['Vi/Vim', 'app-editors/vim', 'app-editors/gvim', ...]
            # into this: [('Vi/Vim', nT), ('app-editors/vim', n1), ('app-editors/gvim', n2), ...]
            # 'nT' is the total percentage of hosts with this app, calculated by
            # chaining Q objects. n1, n2, etc. are also percentages.

            q_list = []
            name, pkgs = split_list(app)

            for index, pkg in enumerate(pkgs):
                q_list.append(Q(installations__package__cp=pkg))

                num = fresh_submissions_qs.filter(installations__package__cp=pkg).count()
                app[index+1] = (pkg, percentify(num, num_hosts))

            q_aggregate = reduce(operator.or_, q_list)
            num_total = fresh_submissions_qs.filter(q_aggregate).distinct().count()
            app[0] = (name, percentify(num_total, num_hosts))

        # sort 'apps' by each app's usage percentage:
        section[1:] = sorted(apps, key=lambda x: x[0][1], reverse=True)

    context = dict(
        num_hosts = num_hosts,
        stats     = json.dumps(stats),
    )

    return render(request, 'stats/app_stats.html', context)
