from django import forms

from .models import Host
from .util import canonicalize_uuid

class HostSearchForm(forms.Form):
    host_id = forms.CharField(label='UUID')

    def clean_host_id(self):
        host_id = canonicalize_uuid(self.cleaned_data['host_id'])

        if not Host.objects.filter(id=host_id).exists():
            raise forms.ValidationError("Invalid/Non-existent UUID.")

        return host_id
