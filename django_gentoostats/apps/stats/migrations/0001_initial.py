# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Category'
        db.create_table(u'stats_category', (
            ('name', self.gf('django.db.models.fields.CharField')(max_length=31, primary_key=True)),
            ('added_on', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'stats', ['Category'])

        # Adding model 'PackageName'
        db.create_table(u'stats_packagename', (
            ('name', self.gf('django.db.models.fields.CharField')(max_length=63, primary_key=True)),
            ('added_on', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'stats', ['PackageName'])

        # Adding model 'Repository'
        db.create_table(u'stats_repository', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=63, db_index=True)),
            ('url', self.gf('django.db.models.fields.CharField')(db_index=True, max_length=255, null=True, blank=True)),
            ('added_on', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'stats', ['Repository'])

        # Adding unique constraint on 'Repository', fields ['name', 'url']
        db.create_unique(u'stats_repository', ['name', 'url'])

        # Adding model 'Package'
        db.create_table(u'stats_package', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(related_name='+', to=orm['stats.Category'])),
            ('package_name', self.gf('django.db.models.fields.related.ForeignKey')(related_name='+', to=orm['stats.PackageName'])),
            ('slot', self.gf('django.db.models.fields.CharField')(max_length=31, null=True, blank=True)),
            ('repository', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='+', null=True, to=orm['stats.Repository'])),
            ('added_on', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('version', self.gf('django.db.models.fields.CharField')(max_length=31)),
            ('cp', self.gf('django.db.models.fields.CharField')(max_length=95, db_index=True)),
        ))
        db.send_create_signal(u'stats', ['Package'])

        # Adding unique constraint on 'Package', fields ['category', 'package_name', 'version', 'slot', 'repository']
        db.create_unique(u'stats_package', ['category_id', 'package_name_id', 'version', 'slot', 'repository_id'])

        # Adding model 'Atom'
        db.create_table(u'stats_atom', (
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(related_name='+', to=orm['stats.Category'])),
            ('package_name', self.gf('django.db.models.fields.related.ForeignKey')(related_name='+', to=orm['stats.PackageName'])),
            ('slot', self.gf('django.db.models.fields.CharField')(max_length=31, null=True, blank=True)),
            ('repository', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='+', null=True, to=orm['stats.Repository'])),
            ('added_on', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('full_atom', self.gf('django.db.models.fields.CharField')(max_length=63, primary_key=True)),
            ('operator', self.gf('django.db.models.fields.CharField')(default='', max_length=2, blank=True)),
            ('version', self.gf('django.db.models.fields.CharField')(max_length=31, blank=True)),
        ))
        db.send_create_signal(u'stats', ['Atom'])

        # Adding unique constraint on 'Atom', fields ['category', 'package_name', 'version', 'slot', 'repository', 'full_atom', 'operator']
        db.create_unique(u'stats_atom', ['category_id', 'package_name_id', 'version', 'slot', 'repository_id', 'full_atom', 'operator'])

        # Adding model 'UseFlag'
        db.create_table(u'stats_useflag', (
            ('name', self.gf('django.db.models.fields.CharField')(max_length=63, primary_key=True)),
            ('added_on', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'stats', ['UseFlag'])

        # Adding model 'Lang'
        db.create_table(u'stats_lang', (
            ('name', self.gf('django.db.models.fields.CharField')(max_length=31, primary_key=True)),
            ('added_on', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'stats', ['Lang'])

        # Adding model 'Host'
        db.create_table(u'stats_host', (
            ('id', self.gf('django.db.models.fields.CharField')(max_length=36, primary_key=True)),
            ('added_on', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('upload_key', self.gf('django.db.models.fields.CharField')(max_length=63)),
        ))
        db.send_create_signal(u'stats', ['Host'])

        # Adding model 'Feature'
        db.create_table(u'stats_feature', (
            ('name', self.gf('django.db.models.fields.CharField')(max_length=63, primary_key=True)),
            ('added_on', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'stats', ['Feature'])

        # Adding model 'MirrorServer'
        db.create_table(u'stats_mirrorserver', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('url', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
            ('added_on', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'stats', ['MirrorServer'])

        # Adding model 'SyncServer'
        db.create_table(u'stats_syncserver', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('url', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
            ('added_on', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'stats', ['SyncServer'])

        # Adding model 'Keyword'
        db.create_table(u'stats_keyword', (
            ('name', self.gf('django.db.models.fields.CharField')(max_length=127, primary_key=True)),
            ('added_on', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'stats', ['Keyword'])

        # Adding model 'Installation'
        db.create_table(u'stats_installation', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('package', self.gf('django.db.models.fields.related.ForeignKey')(related_name='installations', to=orm['stats.Package'])),
            ('keyword', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['stats.Keyword'])),
            ('built_at', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('build_duration', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('size', self.gf('django.db.models.fields.BigIntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'stats', ['Installation'])

        # Adding M2M table for field iuse on 'Installation'
        db.create_table(u'stats_installation_iuse', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('installation', models.ForeignKey(orm[u'stats.installation'], null=False)),
            ('useflag', models.ForeignKey(orm[u'stats.useflag'], null=False))
        ))
        db.create_unique(u'stats_installation_iuse', ['installation_id', 'useflag_id'])

        # Adding M2M table for field pkguse on 'Installation'
        db.create_table(u'stats_installation_pkguse', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('installation', models.ForeignKey(orm[u'stats.installation'], null=False)),
            ('useflag', models.ForeignKey(orm[u'stats.useflag'], null=False))
        ))
        db.create_unique(u'stats_installation_pkguse', ['installation_id', 'useflag_id'])

        # Adding M2M table for field use on 'Installation'
        db.create_table(u'stats_installation_use', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('installation', models.ForeignKey(orm[u'stats.installation'], null=False)),
            ('useflag', models.ForeignKey(orm[u'stats.useflag'], null=False))
        ))
        db.create_unique(u'stats_installation_use', ['installation_id', 'useflag_id'])

        # Adding model 'AtomSet'
        db.create_table(u'stats_atomset', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=127)),
            ('owner', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['stats.Submission'])),
        ))
        db.send_create_signal(u'stats', ['AtomSet'])

        # Adding unique constraint on 'AtomSet', fields ['name', 'owner']
        db.create_unique(u'stats_atomset', ['name', 'owner_id'])

        # Adding M2M table for field atoms on 'AtomSet'
        db.create_table(u'stats_atomset_atoms', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('atomset', models.ForeignKey(orm[u'stats.atomset'], null=False)),
            ('atom', models.ForeignKey(orm[u'stats.atom'], null=False))
        ))
        db.create_unique(u'stats_atomset_atoms', ['atomset_id', 'atom_id'])

        # Adding M2M table for field subsets on 'AtomSet'
        db.create_table(u'stats_atomset_subsets', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('from_atomset', models.ForeignKey(orm[u'stats.atomset'], null=False)),
            ('to_atomset', models.ForeignKey(orm[u'stats.atomset'], null=False))
        ))
        db.create_unique(u'stats_atomset_subsets', ['from_atomset_id', 'to_atomset_id'])

        # Adding model 'Submission'
        db.create_table(u'stats_submission', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('host', self.gf('django.db.models.fields.related.ForeignKey')(related_name='submissions', to=orm['stats.Host'])),
            ('country', self.gf('django.db.models.fields.CharField')(max_length=127, null=True, blank=True)),
            ('added_on', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('protocol', self.gf('django.db.models.fields.IntegerField')()),
            ('raw_data', self.gf('django.db.models.fields.TextField')()),
            ('ip_addr', self.gf('django.db.models.fields.GenericIPAddressField')(max_length=39)),
            ('fwd_addr', self.gf('django.db.models.fields.GenericIPAddressField')(max_length=39, null=True, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=255, null=True, blank=True)),
            ('arch', self.gf('django.db.models.fields.CharField')(max_length=31, null=True, blank=True)),
            ('chost', self.gf('django.db.models.fields.CharField')(max_length=63, null=True, blank=True)),
            ('cbuild', self.gf('django.db.models.fields.CharField')(max_length=63, null=True, blank=True)),
            ('ctarget', self.gf('django.db.models.fields.CharField')(max_length=63, null=True, blank=True)),
            ('platform', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('profile', self.gf('django.db.models.fields.CharField')(max_length=127, null=True, blank=True)),
            ('lang', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='submissions', null=True, to=orm['stats.Lang'])),
            ('lastsync', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('makeconf', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('cflags', self.gf('django.db.models.fields.CharField')(max_length=127, null=True, blank=True)),
            ('cxxflags', self.gf('django.db.models.fields.CharField')(max_length=127, null=True, blank=True)),
            ('ldflags', self.gf('django.db.models.fields.CharField')(max_length=127, null=True, blank=True)),
            ('fflags', self.gf('django.db.models.fields.CharField')(max_length=127, null=True, blank=True)),
            ('sync', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='+', null=True, to=orm['stats.SyncServer'])),
            ('makeopts', self.gf('django.db.models.fields.CharField')(max_length=127, null=True, blank=True)),
            ('emergeopts', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('syncopts', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('acceptlicense', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
        ))
        db.send_create_signal(u'stats', ['Submission'])

        # Adding M2M table for field features on 'Submission'
        db.create_table(u'stats_submission_features', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('submission', models.ForeignKey(orm[u'stats.submission'], null=False)),
            ('feature', models.ForeignKey(orm[u'stats.feature'], null=False))
        ))
        db.create_unique(u'stats_submission_features', ['submission_id', 'feature_id'])

        # Adding M2M table for field mirrors on 'Submission'
        db.create_table(u'stats_submission_mirrors', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('submission', models.ForeignKey(orm[u'stats.submission'], null=False)),
            ('mirrorserver', models.ForeignKey(orm[u'stats.mirrorserver'], null=False))
        ))
        db.create_unique(u'stats_submission_mirrors', ['submission_id', 'mirrorserver_id'])

        # Adding M2M table for field global_use on 'Submission'
        db.create_table(u'stats_submission_global_use', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('submission', models.ForeignKey(orm[u'stats.submission'], null=False)),
            ('useflag', models.ForeignKey(orm[u'stats.useflag'], null=False))
        ))
        db.create_unique(u'stats_submission_global_use', ['submission_id', 'useflag_id'])

        # Adding M2M table for field global_keywords on 'Submission'
        db.create_table(u'stats_submission_global_keywords', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('submission', models.ForeignKey(orm[u'stats.submission'], null=False)),
            ('keyword', models.ForeignKey(orm[u'stats.keyword'], null=False))
        ))
        db.create_unique(u'stats_submission_global_keywords', ['submission_id', 'keyword_id'])

        # Adding M2M table for field installations on 'Submission'
        db.create_table(u'stats_submission_installations', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('submission', models.ForeignKey(orm[u'stats.submission'], null=False)),
            ('installation', models.ForeignKey(orm[u'stats.installation'], null=False))
        ))
        db.create_unique(u'stats_submission_installations', ['submission_id', 'installation_id'])

        # Adding M2M table for field reported_sets on 'Submission'
        db.create_table(u'stats_submission_reported_sets', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('submission', models.ForeignKey(orm[u'stats.submission'], null=False)),
            ('atomset', models.ForeignKey(orm[u'stats.atomset'], null=False))
        ))
        db.create_unique(u'stats_submission_reported_sets', ['submission_id', 'atomset_id'])


    def backwards(self, orm):
        # Removing unique constraint on 'AtomSet', fields ['name', 'owner']
        db.delete_unique(u'stats_atomset', ['name', 'owner_id'])

        # Removing unique constraint on 'Atom', fields ['category', 'package_name', 'version', 'slot', 'repository', 'full_atom', 'operator']
        db.delete_unique(u'stats_atom', ['category_id', 'package_name_id', 'version', 'slot', 'repository_id', 'full_atom', 'operator'])

        # Removing unique constraint on 'Package', fields ['category', 'package_name', 'version', 'slot', 'repository']
        db.delete_unique(u'stats_package', ['category_id', 'package_name_id', 'version', 'slot', 'repository_id'])

        # Removing unique constraint on 'Repository', fields ['name', 'url']
        db.delete_unique(u'stats_repository', ['name', 'url'])

        # Deleting model 'Category'
        db.delete_table(u'stats_category')

        # Deleting model 'PackageName'
        db.delete_table(u'stats_packagename')

        # Deleting model 'Repository'
        db.delete_table(u'stats_repository')

        # Deleting model 'Package'
        db.delete_table(u'stats_package')

        # Deleting model 'Atom'
        db.delete_table(u'stats_atom')

        # Deleting model 'UseFlag'
        db.delete_table(u'stats_useflag')

        # Deleting model 'Lang'
        db.delete_table(u'stats_lang')

        # Deleting model 'Host'
        db.delete_table(u'stats_host')

        # Deleting model 'Feature'
        db.delete_table(u'stats_feature')

        # Deleting model 'MirrorServer'
        db.delete_table(u'stats_mirrorserver')

        # Deleting model 'SyncServer'
        db.delete_table(u'stats_syncserver')

        # Deleting model 'Keyword'
        db.delete_table(u'stats_keyword')

        # Deleting model 'Installation'
        db.delete_table(u'stats_installation')

        # Removing M2M table for field iuse on 'Installation'
        db.delete_table('stats_installation_iuse')

        # Removing M2M table for field pkguse on 'Installation'
        db.delete_table('stats_installation_pkguse')

        # Removing M2M table for field use on 'Installation'
        db.delete_table('stats_installation_use')

        # Deleting model 'AtomSet'
        db.delete_table(u'stats_atomset')

        # Removing M2M table for field atoms on 'AtomSet'
        db.delete_table('stats_atomset_atoms')

        # Removing M2M table for field subsets on 'AtomSet'
        db.delete_table('stats_atomset_subsets')

        # Deleting model 'Submission'
        db.delete_table(u'stats_submission')

        # Removing M2M table for field features on 'Submission'
        db.delete_table('stats_submission_features')

        # Removing M2M table for field mirrors on 'Submission'
        db.delete_table('stats_submission_mirrors')

        # Removing M2M table for field global_use on 'Submission'
        db.delete_table('stats_submission_global_use')

        # Removing M2M table for field global_keywords on 'Submission'
        db.delete_table('stats_submission_global_keywords')

        # Removing M2M table for field installations on 'Submission'
        db.delete_table('stats_submission_installations')

        # Removing M2M table for field reported_sets on 'Submission'
        db.delete_table('stats_submission_reported_sets')


    models = {
        u'stats.atom': {
            'Meta': {'ordering': "['category', 'package_name', 'version', 'slot']", 'unique_together': "(('category', 'package_name', 'version', 'slot', 'repository', 'full_atom', 'operator'),)", 'object_name': 'Atom'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': u"orm['stats.Category']"}),
            'full_atom': ('django.db.models.fields.CharField', [], {'max_length': '63', 'primary_key': 'True'}),
            'operator': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '2', 'blank': 'True'}),
            'package_name': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': u"orm['stats.PackageName']"}),
            'repository': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': u"orm['stats.Repository']"}),
            'slot': ('django.db.models.fields.CharField', [], {'max_length': '31', 'null': 'True', 'blank': 'True'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '31', 'blank': 'True'})
        },
        u'stats.atomset': {
            'Meta': {'unique_together': "(('name', 'owner'),)", 'object_name': 'AtomSet'},
            'atoms': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'parent_set'", 'symmetrical': 'False', 'to': u"orm['stats.Atom']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '127'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['stats.Submission']"}),
            'subsets': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'parent_set'", 'blank': 'True', 'to': u"orm['stats.AtomSet']"})
        },
        u'stats.category': {
            'Meta': {'object_name': 'Category'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '31', 'primary_key': 'True'})
        },
        u'stats.feature': {
            'Meta': {'object_name': 'Feature'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '63', 'primary_key': 'True'})
        },
        u'stats.host': {
            'Meta': {'object_name': 'Host'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.CharField', [], {'max_length': '36', 'primary_key': 'True'}),
            'upload_key': ('django.db.models.fields.CharField', [], {'max_length': '63'})
        },
        u'stats.installation': {
            'Meta': {'ordering': "['package', 'built_at']", 'object_name': 'Installation'},
            'build_duration': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'built_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iuse': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'installations_iuse'", 'blank': 'True', 'to': u"orm['stats.UseFlag']"}),
            'keyword': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['stats.Keyword']"}),
            'package': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'installations'", 'to': u"orm['stats.Package']"}),
            'pkguse': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'installations_pkguse'", 'blank': 'True', 'to': u"orm['stats.UseFlag']"}),
            'size': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'use': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'installations_use'", 'blank': 'True', 'to': u"orm['stats.UseFlag']"})
        },
        u'stats.keyword': {
            'Meta': {'object_name': 'Keyword'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '127', 'primary_key': 'True'})
        },
        u'stats.lang': {
            'Meta': {'object_name': 'Lang'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '31', 'primary_key': 'True'})
        },
        u'stats.mirrorserver': {
            'Meta': {'object_name': 'MirrorServer'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        u'stats.package': {
            'Meta': {'ordering': "['category', 'package_name', 'version', 'slot']", 'unique_together': "(('category', 'package_name', 'version', 'slot', 'repository'),)", 'object_name': 'Package'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': u"orm['stats.Category']"}),
            'cp': ('django.db.models.fields.CharField', [], {'max_length': '95', 'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'package_name': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': u"orm['stats.PackageName']"}),
            'repository': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': u"orm['stats.Repository']"}),
            'slot': ('django.db.models.fields.CharField', [], {'max_length': '31', 'null': 'True', 'blank': 'True'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '31'})
        },
        u'stats.packagename': {
            'Meta': {'object_name': 'PackageName'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '63', 'primary_key': 'True'})
        },
        u'stats.repository': {
            'Meta': {'unique_together': "(('name', 'url'),)", 'object_name': 'Repository'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '63', 'db_index': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'stats.submission': {
            'Meta': {'ordering': "['added_on']", 'object_name': 'Submission'},
            'acceptlicense': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'added_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'arch': ('django.db.models.fields.CharField', [], {'max_length': '31', 'null': 'True', 'blank': 'True'}),
            'cbuild': ('django.db.models.fields.CharField', [], {'max_length': '63', 'null': 'True', 'blank': 'True'}),
            'cflags': ('django.db.models.fields.CharField', [], {'max_length': '127', 'null': 'True', 'blank': 'True'}),
            'chost': ('django.db.models.fields.CharField', [], {'max_length': '63', 'null': 'True', 'blank': 'True'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '127', 'null': 'True', 'blank': 'True'}),
            'ctarget': ('django.db.models.fields.CharField', [], {'max_length': '63', 'null': 'True', 'blank': 'True'}),
            'cxxflags': ('django.db.models.fields.CharField', [], {'max_length': '127', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'emergeopts': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'features': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'submissions'", 'blank': 'True', 'to': u"orm['stats.Feature']"}),
            'fflags': ('django.db.models.fields.CharField', [], {'max_length': '127', 'null': 'True', 'blank': 'True'}),
            'fwd_addr': ('django.db.models.fields.GenericIPAddressField', [], {'max_length': '39', 'null': 'True', 'blank': 'True'}),
            'global_keywords': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'submissions'", 'blank': 'True', 'to': u"orm['stats.Keyword']"}),
            'global_use': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'submissions'", 'blank': 'True', 'to': u"orm['stats.UseFlag']"}),
            'host': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'submissions'", 'to': u"orm['stats.Host']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'installations': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'submissions'", 'blank': 'True', 'to': u"orm['stats.Installation']"}),
            'ip_addr': ('django.db.models.fields.GenericIPAddressField', [], {'max_length': '39'}),
            'lang': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'submissions'", 'null': 'True', 'to': u"orm['stats.Lang']"}),
            'lastsync': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'ldflags': ('django.db.models.fields.CharField', [], {'max_length': '127', 'null': 'True', 'blank': 'True'}),
            'makeconf': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'makeopts': ('django.db.models.fields.CharField', [], {'max_length': '127', 'null': 'True', 'blank': 'True'}),
            'mirrors': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'submissions'", 'blank': 'True', 'to': u"orm['stats.MirrorServer']"}),
            'platform': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'profile': ('django.db.models.fields.CharField', [], {'max_length': '127', 'null': 'True', 'blank': 'True'}),
            'protocol': ('django.db.models.fields.IntegerField', [], {}),
            'raw_data': ('django.db.models.fields.TextField', [], {}),
            'reported_sets': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'submissions'", 'blank': 'True', 'to': u"orm['stats.AtomSet']"}),
            'sync': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': u"orm['stats.SyncServer']"}),
            'syncopts': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'stats.syncserver': {
            'Meta': {'object_name': 'SyncServer'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        u'stats.useflag': {
            'Meta': {'object_name': 'UseFlag'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '63', 'primary_key': 'True'})
        }
    }

    complete_apps = ['stats']