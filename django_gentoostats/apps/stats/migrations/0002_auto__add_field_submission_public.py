# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Submission.public'
        db.add_column(u'stats_submission', 'public',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Submission.public'
        db.delete_column(u'stats_submission', 'public')


    models = {
        u'stats.atom': {
            'Meta': {'ordering': "['category', 'package_name', 'version', 'slot']", 'unique_together': "(('category', 'package_name', 'version', 'slot', 'repository', 'full_atom', 'operator'),)", 'object_name': 'Atom'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': u"orm['stats.Category']"}),
            'full_atom': ('django.db.models.fields.CharField', [], {'max_length': '63', 'primary_key': 'True'}),
            'operator': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '2', 'blank': 'True'}),
            'package_name': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': u"orm['stats.PackageName']"}),
            'repository': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': u"orm['stats.Repository']"}),
            'slot': ('django.db.models.fields.CharField', [], {'max_length': '31', 'null': 'True', 'blank': 'True'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '31', 'blank': 'True'})
        },
        u'stats.atomset': {
            'Meta': {'unique_together': "(('name', 'owner'),)", 'object_name': 'AtomSet'},
            'atoms': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'parent_set'", 'symmetrical': 'False', 'to': u"orm['stats.Atom']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '127'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['stats.Submission']"}),
            'subsets': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'parent_set'", 'blank': 'True', 'to': u"orm['stats.AtomSet']"})
        },
        u'stats.category': {
            'Meta': {'object_name': 'Category'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '31', 'primary_key': 'True'})
        },
        u'stats.feature': {
            'Meta': {'object_name': 'Feature'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '63', 'primary_key': 'True'})
        },
        u'stats.host': {
            'Meta': {'object_name': 'Host'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.CharField', [], {'max_length': '36', 'primary_key': 'True'}),
            'upload_key': ('django.db.models.fields.CharField', [], {'max_length': '63'})
        },
        u'stats.installation': {
            'Meta': {'ordering': "['package', 'built_at']", 'object_name': 'Installation'},
            'build_duration': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'built_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iuse': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'installations_iuse'", 'blank': 'True', 'to': u"orm['stats.UseFlag']"}),
            'keyword': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['stats.Keyword']"}),
            'package': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'installations'", 'to': u"orm['stats.Package']"}),
            'pkguse': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'installations_pkguse'", 'blank': 'True', 'to': u"orm['stats.UseFlag']"}),
            'size': ('django.db.models.fields.BigIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'use': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'installations_use'", 'blank': 'True', 'to': u"orm['stats.UseFlag']"})
        },
        u'stats.keyword': {
            'Meta': {'object_name': 'Keyword'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '127', 'primary_key': 'True'})
        },
        u'stats.lang': {
            'Meta': {'object_name': 'Lang'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '31', 'primary_key': 'True'})
        },
        u'stats.mirrorserver': {
            'Meta': {'object_name': 'MirrorServer'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        u'stats.package': {
            'Meta': {'ordering': "['category', 'package_name', 'version', 'slot']", 'unique_together': "(('category', 'package_name', 'version', 'slot', 'repository'),)", 'object_name': 'Package'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': u"orm['stats.Category']"}),
            'cp': ('django.db.models.fields.CharField', [], {'max_length': '95', 'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'package_name': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': u"orm['stats.PackageName']"}),
            'repository': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': u"orm['stats.Repository']"}),
            'slot': ('django.db.models.fields.CharField', [], {'max_length': '31', 'null': 'True', 'blank': 'True'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '31'})
        },
        u'stats.packagename': {
            'Meta': {'object_name': 'PackageName'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '63', 'primary_key': 'True'})
        },
        u'stats.repository': {
            'Meta': {'unique_together': "(('name', 'url'),)", 'object_name': 'Repository'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '63', 'db_index': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'stats.submission': {
            'Meta': {'ordering': "['added_on']", 'object_name': 'Submission'},
            'acceptlicense': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'added_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'arch': ('django.db.models.fields.CharField', [], {'max_length': '31', 'null': 'True', 'blank': 'True'}),
            'cbuild': ('django.db.models.fields.CharField', [], {'max_length': '63', 'null': 'True', 'blank': 'True'}),
            'cflags': ('django.db.models.fields.CharField', [], {'max_length': '127', 'null': 'True', 'blank': 'True'}),
            'chost': ('django.db.models.fields.CharField', [], {'max_length': '63', 'null': 'True', 'blank': 'True'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '127', 'null': 'True', 'blank': 'True'}),
            'ctarget': ('django.db.models.fields.CharField', [], {'max_length': '63', 'null': 'True', 'blank': 'True'}),
            'cxxflags': ('django.db.models.fields.CharField', [], {'max_length': '127', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'emergeopts': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'features': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'submissions'", 'blank': 'True', 'to': u"orm['stats.Feature']"}),
            'fflags': ('django.db.models.fields.CharField', [], {'max_length': '127', 'null': 'True', 'blank': 'True'}),
            'fwd_addr': ('django.db.models.fields.GenericIPAddressField', [], {'max_length': '39', 'null': 'True', 'blank': 'True'}),
            'global_keywords': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'submissions'", 'blank': 'True', 'to': u"orm['stats.Keyword']"}),
            'global_use': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'submissions'", 'blank': 'True', 'to': u"orm['stats.UseFlag']"}),
            'host': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'submissions'", 'to': u"orm['stats.Host']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'installations': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'submissions'", 'blank': 'True', 'to': u"orm['stats.Installation']"}),
            'ip_addr': ('django.db.models.fields.GenericIPAddressField', [], {'max_length': '39'}),
            'lang': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'submissions'", 'null': 'True', 'to': u"orm['stats.Lang']"}),
            'lastsync': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'ldflags': ('django.db.models.fields.CharField', [], {'max_length': '127', 'null': 'True', 'blank': 'True'}),
            'makeconf': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'makeopts': ('django.db.models.fields.CharField', [], {'max_length': '127', 'null': 'True', 'blank': 'True'}),
            'mirrors': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'submissions'", 'blank': 'True', 'to': u"orm['stats.MirrorServer']"}),
            'platform': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'profile': ('django.db.models.fields.CharField', [], {'max_length': '127', 'null': 'True', 'blank': 'True'}),
            'protocol': ('django.db.models.fields.IntegerField', [], {}),
            'public': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'raw_data': ('django.db.models.fields.TextField', [], {}),
            'reported_sets': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'submissions'", 'blank': 'True', 'to': u"orm['stats.AtomSet']"}),
            'sync': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': u"orm['stats.SyncServer']"}),
            'syncopts': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'stats.syncserver': {
            'Meta': {'object_name': 'SyncServer'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        u'stats.useflag': {
            'Meta': {'object_name': 'UseFlag'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '63', 'primary_key': 'True'})
        }
    }

    complete_apps = ['stats']