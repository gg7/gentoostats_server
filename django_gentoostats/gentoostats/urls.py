from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url( r'^upload(/|$)'
       , include( 'apps.receiver.urls'
                , namespace = 'receiver'
                , app_name  = 'receiver'
         )
    ),

    url( r''
       , include( 'apps.stats.urls'
                , namespace = 'stats'
                , app_name  = 'stats'
         )
    ),
)
