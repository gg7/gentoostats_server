from .base import *

DEBUG = False

ALLOWED_HOSTS = [
    '.gentoostats.com', # Allow domain and subdomains
]
