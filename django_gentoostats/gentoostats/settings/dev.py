from .base import *

DEBUG = True

INTERNAL_IPS = ("127.0.0.1",)

INSTALLED_APPS += (
    # 'django.contrib.admin',
    # 'django.contrib.admindocs',
    'django_extensions',
    'debug_toolbar',
    'cache_panel',
)

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
   }
}

MIDDLEWARE_CLASSES += (
    'middleware.DisableClientSideCachingMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

DEBUG_TOOLBAR_CONFIG = {
    'INTERCEPT_REDIRECTS': True,
}

DEBUG_TOOLBAR_PANELS = (
    'debug_toolbar.panels.version.VersionDebugPanel',
    'debug_toolbar.panels.timer.TimerDebugPanel',
    'debug_toolbar.panels.settings_vars.SettingsVarsDebugPanel',
    'debug_toolbar.panels.headers.HeaderDebugPanel',
    'debug_toolbar.panels.request_vars.RequestVarsDebugPanel',
    'debug_toolbar.panels.sql.SQLDebugPanel',
    'debug_toolbar.panels.template.TemplateDebugPanel',
    'debug_toolbar.panels.signals.SignalDebugPanel',
    'debug_toolbar.panels.logger.LoggingPanel',
    'cache_panel.panel.CacheDebugPanel',
)

# SHOW_TOOLBAR_CALLBACK = lambda request: True
