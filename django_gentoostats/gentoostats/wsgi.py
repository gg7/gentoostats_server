"""
WSGI config for django_gentoostats project.

This module contains the WSGI application used by Django's development server
and any production WSGI deployments. It should expose a module-level variable
named ``application``. Django's ``runserver`` and ``runfcgi`` commands discover
this application via the ``WSGI_APPLICATION`` setting.

Usually you will have the standard Django WSGI application here, but it also
might make sense to replace the whole Django WSGI application with a custom one
that later delegates to the Django one. For example, you could introduce WSGI
middleware here, or combine a Django application with an application of another
framework.

"""
import os
import sys

# default settings module to use
os.environ.setdefault("DJANGO_SETTINGS_MODULE",
                      "gentoostats.settings.production")

# Re: mod_wsgi and virtualenv:
#   Since we can only specify which Python interpreter to use during compile
# time (ideally that would be ${project_venv}/bin/python), we have to manually
# add the 'site-packages' directory of the virtualenv to sys.path. The
# activate_this script does that for us. It even reorders sys.path and updates
# system.prefix. This can, however, lead to some problems with mod_wsgi.
# See https://code.google.com/p/modwsgi/wiki/VirtualEnvironments .

# Activate the virtualenv
# activate_this = '{{ project_venv }}/bin/activate_this.py'
# execfile(activate_this, dict(__file__=activate_this))

# print >>sys.stderr, 'path 1: ' + str(sys.path)

# add the project directory to sys.path:
project_path = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
if not project_path in sys.path:
    # https://code.google.com/p/modwsgi/wiki/ConfigurationDirectives#WSGIReloadMechanism
    # NOTE: .pth files won't be processed
    sys.path.insert(0, project_path)

# print >>sys.stderr, 'path 2: ' + str(sys.path)

# This application object is used by any WSGI server configured to use this
# file. This includes Django's development server, if the WSGI_APPLICATION
# setting points here.
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

# Apply WSGI middleware here.
# from helloworld.wsgi import HelloWorldApplication
# application = HelloWorldApplication(application)
